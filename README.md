# RenderService plugin for Chunky

## Installation

1. Download the latest version of the RenderService plugin
2. Move the jar into the Chunky plugin directory (`%APPDATA%\.chunky\plugins` on Windows, `~/.chunky/plugins` on Linux) to
    include the plugin. Create the plugin directory if it doesn't exist.
3. Launch Chunky as usual. A new tab in the renderer will magically appear!