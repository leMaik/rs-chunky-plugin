/*
 * Copyright (c) 2016 Wertarbyte <http://wertarbyte.com>
 *
 * This file is part of the RenderService.
 *
 * Wertarbyte RenderService is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wertarbyte RenderService is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wertarbyte RenderService.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.chunkyplugin;

import com.sun.deploy.uitoolkit.impl.fx.HostServicesFactory;
import com.wertarbyte.renderservice.chunkyplugin.rendering.ApiClient;
import com.wertarbyte.renderservice.chunkyplugin.rendering.RenderJob;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.Region;
import se.llbit.chunky.main.Chunky;
import se.llbit.chunky.renderer.RenderContext;
import se.llbit.chunky.renderer.scene.Scene;
import se.llbit.chunky.ui.ChunkyFx;
import se.llbit.chunky.ui.render.RenderControlsTab;
import se.llbit.json.JsonValue;
import se.llbit.util.ProgressListener;
import se.llbit.util.TaskTracker;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class RenderServiceTab implements RenderControlsTab {
    private static final String RS3_API_URL = "https://api.rs.wertarbyte.com";
    private final Chunky chunky;
    private Scene scene;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label progressLabel;
    @FXML
    private Button uploadButton;

    public RenderServiceTab(Chunky chunky) {
        this.chunky = chunky;
    }

    @Override
    public void update(Scene scene) {
        this.scene = scene;
        Platform.runLater(() -> {
            uploadButton.setDisable(scene == null);
        });
    }

    @Override
    public Tab getTab() {
        Tab tab = new Tab("RenderService");

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/rs-chunky-plugin.fxml"));
            fxmlLoader.setController(this);
            tab.setContent(fxmlLoader.load());
            return tab;
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize RenderService plugin", e);
        }
    }

    @FXML
    private void uploadScene(ActionEvent event) {
        if (this.scene != null) {
            uploadButton.setDisable(true);

            TaskTracker taskTracker = new TaskTracker(
                    new ProgressListener() {
                        @Override
                        public void setProgress(String task, int done, int start, int target) {
                            Platform.runLater(() -> {
                                progressLabel.setText(task + " - " + done + " of " + target);
                                if (target > 0 && done <= target) {
                                    progressBar.setProgress((double) done / target);
                                }
                            });
                        }

                        @Override
                        public void setProgress(String task, int done, int start, int target, String eta) {
                            setProgress(task, done, start, target);
                        }
                    },
                    (tracker, previous, name, size) -> new TaskTracker.Task(tracker, previous, name, size) {
                        @Override
                        public void close() {
                            Platform.runLater(() -> {
                                progressLabel.setText("");
                                progressBar.setProgress(0);
                            });
                        }
                    });

            CompletableFuture.runAsync(() -> {
                File tempDir;
                try {
                    tempDir = Files.createTempDirectory("chunky-rs3").toFile();
                    System.out.println(tempDir.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                try {
                    scene.saveScene(new RenderContext(chunky) {
                        @Override
                        public File getSceneDirectory() {
                            return tempDir;
                        }

                        @Override
                        public File getSceneFile(String fileName) {
                            return new File(getSceneDirectory(), fileName);
                        }
                    }, taskTracker);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }

                try {
                    JsonValue skymap = scene.sky().toJson().get("skymap");
                    RenderJob job = new ApiClient(RS3_API_URL).createJob(
                            new File(tempDir, scene.name() + ".json"),
                            new File(tempDir, scene.name() + ".octree"),
                            new File(tempDir, scene.name() + ".grass"),
                            new File(tempDir, scene.name() + ".foliage"),
                            skymap != null && skymap.stringValue(null) != null ? new File(skymap.stringValue(null)) : null,
                            taskTracker
                    ).get();

                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Scene uploaded");
                        alert.setHeaderText("The scene was successfully uploaded");
                        alert.setContentText("A new render job with the ID " + job.getId() + " was created and will be processed as soon as possible.");
                        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE); // Linux text wrap work-around
                        ButtonType close = ButtonType.CLOSE;
                        ButtonType show = new ButtonType("Show job");
                        alert.getButtonTypes().setAll(close, show);
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent() && result.get() == show) {
                            HostServicesFactory.getInstance(new ChunkyFx()).showDocument("https://api.rs.wertarbyte.com/jobs/" + job.getId() + "/latest.png");
                        }
                    });
                } catch (IOException | InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }

                Platform.runLater(() -> {
                    uploadButton.setDisable(false);
                });
            });
        }
    }

    @FXML
    private void websiteHyperlinkClicked(ActionEvent event) throws URISyntaxException, IOException {
        HostServicesFactory.getInstance(new ChunkyFx()).showDocument("https://rs.wertarbyte.com");
    }
}
